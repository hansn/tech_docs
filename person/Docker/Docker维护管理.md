# PHP容器构建

*参考：*  
*docker指令 <https://docs.docker.com/engine/reference/commandline/docker/>*  

## Docker清理

`docker system prune`

> 清理为使用的容器、镜像、网络，可选的还有卷

<https://docs.docker.com/engine/reference/commandline/system_prune/>

