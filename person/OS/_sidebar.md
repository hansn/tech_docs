Linux
  * [README](/person/OS/Linux/)
  * [系统时间管理](/person/OS/Linux/系统时间管理)
  * [crontab定时任务](/person/OS/Linux/crontab)
  * [Linux用户组和权限管理](/person/OS/Linux/Linux用户组和权限管理)
  * [软件镜像站](/person/OS/Linux/软件镜像站)
---
Windows
  * [Win10常见功能开启](/person/OS/Win/Win10常见功能开启)
  * [Win10开机启动脚本](/person/OS/Win/Win10开机启动脚本)
