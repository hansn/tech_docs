# 自然指数

## 推导过程

自然增长的极限

### 定义

$$
e = \lim\limits_{n\to\infty}(1+\frac{1}{n}) ^n
$$

### 级数表达式

$$
e = \sum_{n=0}^\infty \frac{1}{n!} = \frac{1}{0!} + \frac{1}{1!}+ \frac{1}{2!}+ \frac{1}{3!}+ \frac{1}{4!} + ...
$$

$$
e ^ x = \sum_{n=0}^\infty\frac{x^n}{n!}
$$


### 欧拉恒等式

$$
e^{i\pi} + 1 = 0
$$

### 三角公式

$$
e ^ {i\theta} = \cos\theta + \sin\theta
$$

令

$$ \theta = \pi$$

得到等式

$$ e ^ {i\pi} = -1 $$