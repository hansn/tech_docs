# Supported Functions

This is a list of TeX functions supported by KaTeX. It is sorted into logical groups.

There is a similar Support Table, sorted alphabetically, that lists both supported and un-supported functions.

Accents

||||
| -- | -- | -- |
|$$ a' $$ `a'`| $$ \tilde{a} $$ `\tilde{a}` | $$ \mathring{g} $$ `\mathring{g}` |
|$$ a'' $$`a''`| $$ \widetilde{ac} $$ `\widetilde{ac}` | $$ \overgroup{AB} $$ `\overgroup{AB}` |
|$$ a^{\prime} $$ `a^{\prime}` | $$ \utilde{AB} $$ `\utilde{AB}` | $$ \undergroup{AB} $$ `\undergroup{AB}`|
|$$ \acute{a} $$`\acute{a}` | $$ \vec{F} $$ `\vec{F}` | $$ \Overrightarrow{AB} $$ `\Overrightarrow{AB}`|
|$$ \bar{y} $$ `\bar{y}` | $$ \overleftarrow{AB} $$ `\overleftarrow{AB}` | $$ \overrightarrow{AB} $$ `\overrightarrow{AB}`|
|$$ \breve{a} $$ `\breve{a}` | $$ \underleftarrow{AB} $$ `\underleftarrow{AB}` | $$ \underrightarrow{AB} $$ `\underrightarrow{AB}`|
|$$ \check{a} $$ `\check{a}` | $$ \overleftharpoon{ac} $$ `\overleftharpoon{ac}` | $$ \overrightharpoon{ac} $$ `\overrightharpoon{ac}`|
|$$ \dot{a} $$`\dot{a}` | $$ \overleftrightarrow{AB} $$ `\overleftrightarrow{AB}` | $$ \overbrace{AB} $$ `\overbrace{AB}`|
|$$ \ddot{a} $$ `\ddot{a}` | $$ \underleftrightarrow{AB} $$ `\underleftrightarrow{AB}` | $$ \underbrace{AB} $$ `\underbrace{AB}`|
|$$ \grave{a} $$`\grave{a}` | $$ \overline{AB} $$ `\overline{AB}` | $$ \overlinesegment{AB} $$ `\overlinesegment{AB}`|
|$$ \hat{\theta} $$ `\hat{\theta}` | $$ \underline{AB} $$ `\underline{AB}` | $$ \underlinesegment{AB} $$ `\underlinesegment{AB}`|
|$$ \widehat{ac} $$`\widehat{ac}` | $$ \widecheck{ac} $$ `\widecheck{ac}` | $$ \underbar{X} $$ `\underbar{X}`|

Accent functions inside \text{…}

|||||
|--|--|--|--|
|$$ \\'{a} $$ `\'{a}`|$$ \\\~{a} $$ `\~{a}`| $$ \\.{a} $$ `\.{a}` |$$ \H{a} $$ `\H{a}` |
|$$ \\`{a} $$`` \`{a} ``|$$ \\={a} $$ `\={a}`| $$ \\"{a} $$ `\"{a}` |$$ \v{a} $$ `\v{a}` |
|$$ \\^{a} $$`\^{a}`|$$ \\u{a} $$ `\u{a}`| $$ \\r{a} $$ `\r{a}` ||

## Delimiters

||||||
|--|--|--|--|--|
|$$ ( ) $$`( )`|$$ \lparen \rparen $$`\lparen \rparen`|$$⌈ ⌉$$`⌈ ⌉`|$$ \lceil \rceil $$`\lceil \rceil`|$$ \uparrow $$`\uparrow`|
|$$[ ]$$`[ ]`|$$\lbrack \rbrack$$`\lbrack \rbrack`|$$⌊ ⌋$$`⌊ ⌋`|$$\lfloor \rfloor$$`\lfloor \rfloor`|$$\downarrow$$`\downarrow`|
|$$\{ \}$$`\{ \}`|$$\lbrace \rbrace$$`\lbrace \rbrace`|$$⎰⎱$$`⎰⎱`|$$\lmoustache \rmoustache$$`\lmoustache \rmoustache`|$$\updownarrow$$`\updownarrow`|
|$$⟨ ⟩$$`⟨ ⟩`|$$\langle \rangle$$`\langle \rangle`|$$⟮ ⟯$$`⟮ ⟯`|$$\lgroup \rgroup$$`\lgroup \rgroup`|$$\Uparrow$$`\Uparrow`|
|$$|$$`|`|$$\vert$$`\vert`|$$┌ ┐$$`┌ ┐`|$$\ulcorner \urcorner$$`\ulcorner \urcorner`|$$\Downarrow$$`\Downarrow`|
|$$\|$$`\|`|$$\Vert$$`\Vert`|$$└ ┘$$`└ ┘`|$$\llcorner \lrcorner$$`\llcorner \lrcorner`|$$\Updownarrow$$`\Updownarrow`|
|$$\lvert \rvert$$`\lvert \rvert`|$$\lVert \rVert$$`\lVert \rVert`| | |$$\backslash$$`\backslash`|
|$$\lang \rang$$`\lang \rang`|$$\lt \gt$$`\lt \gt`|$$⟦ ⟧$$`⟦ ⟧`|$$\llbracket \rrbracket$$`\llbracket \rrbracket`|$$\lBrace \rBrace$$`\lBrace \rBrace`|

